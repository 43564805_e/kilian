# curriculum vitae #
![](imagen/f1.png)
## Datos personales: ##
**Nombre y Apellido:** Kilian Reymundo

**Teléfono:** 658927003

**Dirección correo electrónico:**  [kil_80@hotmail.com](kil_80@hotmail.com)

**Fecha de nacimiento:** 19/09/1989

## Formación Académica: ##

**Prueba de acceso a grado medio:** Centre Joan D'Austria

**Any:** 2005-2006

**Grado medio de Informática y Microredes** (2017-2019)

**Centre IES Poblenou**

## Experiencia Profesional ##
**Zipbcn Solutions S.L.:** Encargado de almacén (2014-2017)

*Preparación de albaranes

*Control de estoc

*Logística

**IKEA:** Logística de almacén Barcelona 2011-2014

*Embalaje

*Preparación de albaranes

*Cara al publico

**Supermercado consum:** Barcelona 2007-2008

*Reponedor

*Cajero

*Dependiente


## Conocimientos adicionales ##

Idioma | Comprensión oral | comprensión escrita | expresión oral | expresión escrita
---|---|---|---|---
Catalán | muy bien | muy bien |  muy bien | muy bien | muy bien 
castellano | muy bien | muy bien | muy bien | muy bien | muy bien 
Inglés | basico | basico | basico | basico 



## Capacidades y habilidades adquiridas ##

+ Soy una persona rsponsable y puntual

+ Me gusta trabajar en equipo

+ Aprendo de mis errores y tengo interes por adquirir conocimentos nuevos.

+ Disponibilidad horaria

