# Què és HTML, història i  per a què serveix?


### Història:

**HTML** va néixer públicament en un document anomenat HTML Tags (etiquetes HTML), publicat per primera vegada a Internet per [Tim Berners-Lee](https://ca.wikipedia.org/wiki/Tim_Berners-Lee) en 1991. En aquesta publicació es descriuen 22 etiquetes que mostraven un disseny inicial i relativament simple d'HTML. Diversos d'aquests elements es conserven en l'actualitat; altres s'han deixat d'usar, i molts altres s'han anat afegint amb el pas dels anys. D'aquesta manera, podem parlar que han existit diferents versions d'HTML al llarg de la història d'internet.

### Què és i per a què serveix?

**HTML**, de l’anglès **HyperText Markup Language** és el llenguatge que s’empra per al desenvolupament de pàgines d'Internet. 
Està compost per una sèrie d'etiquetes que el navegador interpreta, i dóna forma a la pantalla. HTML disposa d'etiquetes per a imatges, hipervincles que ens permeten dirigir-nos a altres pàgines, salts de línia, llistes, taules, etc.

*Per exemple, tot document Html comença amb:*

**< !DOCTYPE html >**

**< html >**

Bàsicament el llenguatge HTML serveix per descriure l’estructura bàsica d’una pàgina i organitzar la forma en què es mostrarà el seu contingut, a més a més, el HTML permet incloure enllaços (links) cap a altres pàgines o documents.

![Alt](imagen/HTML_source_code_example.svg.png "Source Code HTML")

Com podem observar en aquesta imatge, cadascuna de les etiquetes ha d'acabar amb la seva homòloga de tancament. En aquest cas l'etiqueta < html > s'ha de tancar amb < / html >; l'etiqueta < body > amb </ body>, i la < p > amb </ p>.

